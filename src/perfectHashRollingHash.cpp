/**
  Solution with support for variable shapes.

  Perfect hash implementation of finding k-mers.
  Construction of index is of complexity O(N*K) where K is length of
  shape (k-mer).

  In this solution, opposed to suffix array, we need to construct whole index
  from beginning every time shape is changed.

  Calculating hash: to code one base we use 2 bits. This way size of index is
  2^(2*(K - x)), where x is number of DC positions.

    + Implementation of index
  During index construction we use array of vectors for storing multiple
  positions for current hash. After whole construction is done, we can store
  those positions in two arrays, MReferences and MPositions:
    1) MPositions holds positions of same hash stored sequentially (positions
       of same hash are always next to each other) and
    2) MReferences holds for each hash a index of first position in MPositions
       where its positions are stored.
  This way we can fastly approach to every hash and calculate the number of
  matching positions by subtracting two consecutive elementes in MReferences.

    + Index construction:
  We need to handle 4 cases with DC positions. Let's say our shape
  is '00100' and our reference is ATCGT:
    - match     (ATCGT)
    - missmatch (ATXGT)
    - deletion  (ATGT)
    - insertion (ATXXGT) (we allow missmatch on this position also)
  We can easily deduce that all those cases can be compressed to one: ATGT.
  So, when putting every substring from reference in hash, we just skip bases
  which are DC.

    + K-mer searching
  When searching some k-mer in reference, we need to have three different queries
  in index: one for match/missmatch, one for deletion and one for insertion case.
  Let's say our shape is '00100' and query is ACGTAT. Following k-mers are:
    - match/missmatch     (ACXTA -> ACTA)
    - deletion            (ACXGT -> ACGT) - in query one base was deleted
    - insertion+missmatch (ACTAT -> ACAT) - in query one base was added (in
      this case 'G') so after removing it we allow missmatch (that is way T is
      also removed)

    + Rolling hash
  Calculation of rolling hash is done by using blocks of hashes.
  Block is sequence of consequtive match positions (zeros - 0) in shape.

*/

#include <cstdio>
#include <vector>
#include <cstring>
#include <ctime>

#define WINVER 0x0500
#include <windows.h>
#include <psapi.h>
using namespace std;

/*******************************/
/// CONSTANTS
/*******************************/

#define MAXN 1000000
#define QUERY 100000
#define HASH (1 << 26)
#define SHAPE 30
#define MAXPOW 20
#define BASE 4

/*******************************/
/// DATA STRUCTURES
/*******************************/

char reference[MAXN + 1];
char query[QUERY + 1];
char shape[SHAPE + 1 + 1];
vector<int> M[HASH + 1];
int MReferences[HASH + 1];
int MPositions[HASH];
int shapeLen;
int hashLen;
int n, m;
int k;
int hashMis, hashDel, hashIns;
int posMMBlock[SHAPE], lenMMBlock[SHAPE], powMMBlock[SHAPE], hashMMBlock[SHAPE];
int posINSBlock[SHAPE], lenINSBlock[SHAPE], powINSBlock[SHAPE], hashINSBlock[SHAPE];
int blocks;

/*******************************/
/// OPERATIONAL FUNCTIONS
/*******************************/

/*
  Calculates number of positions in reference for given hash.
*/
inline int calculateSize(int hash) {
   return MReferences[hash + 1] - MReferences[hash];
}

/*
  Output found matches for given hash.
*/
void outputFoundMatches(int hash) {
  int size = calculateSize(hash);
  for (int i = 0; i < size; i++) {
    printf ("%d\n", MPositions[MReferences[hash] + i]);
  }
}

/*
  Calculates the mismatch hash of given string of positions [left, right> : used only in index construction.
*/
int calculateHashMis(char *string, int left, int right) {
  int hash = 0;
  for (int i = 0, length = right - left; i < length; i++) {
    if (shape[i] == '1') { // skipping DC positions
      continue;
    }
    hash <<= 2;
    //hash += calculateBase(&string[left + i]);
    hash += string[left + i];
  }
  return hash;
}

/*
  Calculates the deletion hash of given string from given position: used only for first deletion hash in rolling hash.
*/
int calculateHashDel(char *string, int position) {
  int hash = 0;
  for (int i = 0; i < hashLen; i++) {
    hash <<= 2;
    //hash += calculateBase(&string[position + i]);
    hash += string[position + i];
  }
  return hash;
}

/*
  Calculates the rolling deletion hash of given string from given position and from previous hash.
*/
int calculateHashDelRolling(int hash, char *string, int position) {
  // mozda obrnut redoslijed!!!
  hash -= string[position - 1] << (2 * (hashLen - 1));
  hash <<= 2;
  hash += string[position + hashLen - 1];
  return hash;
}

/*
  Calculates hole hash for given hash blocks in match/missmatch and insertion cases: used only for first hashes.
*/
int calculateHoleHash(int position, char *string, int *positions, int *lengths, int *powers, int *hashs, int size) {
  int hash = 0;
  for (int i = 0; i < size; i++) {
    hashs[i] = 0;
    /*printf ("za poziciju: %d\n", position);
    printf ("pos: %d\n", positions[i]);
    printf ("len: %d\n", lengths[i]);
    printf ("pow: %d\n", powers[i]);*/
    for (int j = 0; j < lengths[i]; j++) {
      hashs[i] <<= 2;
      hashs[i] += string[position + j + positions[i]];
    }
    hash += hashs[i] << (2 * powers[i]);
  }
  return hash;
}

/*
  Calculates rolling hash for given hash blocks from prevoius hash.
*/
int calculateRollingHash(int position, char *string, int *positions, int *lengths, int *powers, int *hashs, int size) {
  int hash = 0;
  for (int i = 0; i < size; i++) {
    hashs[i] -= string[position - 1 + positions[i]] << (2 * (lengths[i] - 1));
    hashs[i] <<= 2;
    hashs[i] += string[position + positions[i] + lengths[i] - 1];
    hash += hashs[i] << (2 * powers[i]);
  }
  return hash;
}

/*
  Finds all hits of given string (query) in reference from positions [left, right>.
*/
void findMathces(char *string, int left, int right) {
  if (left == 0) {
    hashMis = calculateHoleHash(left, string, posMMBlock, lenMMBlock, powMMBlock, hashMMBlock, blocks);
  } else {
    hashMis = calculateRollingHash(left, string, posMMBlock, lenMMBlock, powMMBlock, hashMMBlock, blocks);
  }
  int foundMis = calculateSize(hashMis);
  //printf ("MM:\n");
  printf ("%d\n", foundMis);
  outputFoundMatches(hashMis);
  //printf ("--\n");

  //printf ("DEL:\n");
  if (left == 0) {
    hashDel = calculateHashDel(string, left);
  } else {
    hashDel = calculateHashDelRolling(hashDel, string, left);
  }
  int foundDel = calculateSize(hashDel);
  printf ("%d\n", foundDel);
  outputFoundMatches(hashDel);
  //printf ("--\n");

  //printf ("INS:\n");
  if (left + shapeLen + k <= m) {
    if (left == 0) {
      hashIns = calculateHoleHash(left, string, posINSBlock, lenINSBlock, powINSBlock, hashINSBlock, blocks);
    } else {
      hashIns = calculateRollingHash(left, string, posINSBlock, lenINSBlock, powINSBlock, hashINSBlock, blocks);
    }
    int foundIns = calculateSize(hashIns);
    printf ("%d\n", foundIns);
    outputFoundMatches(hashIns);
  } else {
    printf ("0\n");
  }

  //printf ("---------\n");
}

/*******************************/
/// PRECOMPUTATION
/*******************************/

/*
  Constructs index.
*/
void constructIndex() {
  for (int i = 0; i + shapeLen <= n; i++) {
    int hash = calculateHashMis(reference, i, i + shapeLen);
    M[hash].push_back(i);
  }

  // now me make static structures - arrays - (because data will not change)
  // which are faster than vector

  MReferences[0] = 0;
  for (int i = 1; i <= HASH; i++) {
    MReferences[i] = MReferences[i - 1] + M[i - 1].size();
  }

  for (int i = 0; i < HASH; i++) {
    for (int j = 0, d = M[i].size(); j < d; j++) {
      MPositions[MReferences[i] + j] = M[i][j];
    }
  }
}

/*
  Codes given base to its equivalent number representation.
*/
int calculateBase(char *base) {
  char _base = *base;
  switch (_base) {
    case 'A': return 0;
    case 'C': return 1;
    case 'T': return 2;
    case 'G': return 3;
  }
}

/*
  Precalculation of bases of reference and query to avoid repeating conversion in later steps.
*/
void convertBasesValue(void) {
  for (int i = 0; i < n; i++) {
    reference[i] = calculateBase(&reference[i]);
  }
  for (int i = 0; i < m; i++) {
    query[i] = calculateBase(&query[i]);
  }
}

/*
  Calculates number of blocks in shape.
*/
void calculateNumberOfBlocks(void) {
  bool block = false;
  for (int i = 0; i < shapeLen; i++) {
    if (shape[i] == '0' && block == false) {
      block = true;
      blocks++;
    } else if (shape[i] == '0' && block == true) {
      // nothing
    } else if (shape[i] == '1' && block == true) {
      block = false;
    } else { // shape[i] == '1' && block == false
      // nothing
    }
  }
}

/*
  Calculates match/mismatch blocks in shape and all informations needed for efficiently calculating the rolling hash.
*/
void calculateMMBlocks(void) {
  bool block = false;
  int blocksMM = 0;
  shape[shapeLen] = '1';
  shape[shapeLen + 1] = '\0';
  for (int i = 0; i <= shapeLen; i++) {
    if (shape[i] == '0'&& block == false) {
      block = true;
      posMMBlock[blocksMM] = i;
      lenMMBlock[blocksMM] = 1;
    } else if (shape[i] == '0' && block == true) {
      lenMMBlock[blocksMM]++;
    } else if (shape[i] == '1' && block == true) {
      block = false;
      blocksMM++;
    } else { // shape[i] == '1' && block == false
      // nothing
    }
  }
  shape[shapeLen] = '\0';
  blocksMM--;
  block = false;
  int power = 0;
  for (int i = shapeLen - 1; i >= 0; i--) {
    if (shape[i] == '0' && block == false) {
      block = true;
      powMMBlock[blocksMM] = power;
      power++;
    } else if (shape[i] == '0' && block == true) {
      power++;
    } else if (shape[i] == '1' && block == true) {
      block = false;
      blocksMM--;
    } else { // shape[i] == '1' && block == false
      // nothing
    }
  }
}

/*
  Calculates insertion blocks in shape and all informations needed for efficently calculating the rolling hash.
*/
void calculateINSBlocks(void) {
  int blocksINS = 0;
  bool block = false;
  shape[shapeLen] = '1';
  for (int i = 0, j = 0; i <= shapeLen; i++, j++) {
    if (shape[i] == '0' && block == false) {
      block = true;
      posINSBlock[blocksINS] = j;
      lenINSBlock[blocksINS] = 1;
    } else if (shape[i] == '0' && block == true) {
      lenINSBlock[blocksINS]++;
    } else if (shape[i] == '1' && block == true) {
      block = false;
      blocksINS++;
      j++;
    } else { // shape[i] == '1' && block == false
      j++;
    }
  }
  shape[shapeLen] = '\0';
  blocksINS--;
  block = false;
  int power = 0;
  for (int i = shapeLen - 1; i >= 0; i--) {
    if (shape[i] == '0' && block == false) {
      block = true;
      powINSBlock[blocksINS] = power;
      power++;
    } else if (shape[i] == '0' && block == true) {
      power++;
    } else if (shape[i] == '1' && block == true) {
      block = false;
      blocksINS--;
    } else { // shape[i] == '1' && block == false
      // nothnig
    }
  }
}

/*
  Runs functions for blocks calulation needed for calculating rolling hash online.
*/
void calculateBlocks(void) {
  calculateNumberOfBlocks();
  calculateMMBlocks();
  calculateINSBlocks();
}

/*
  Calculates the lenght of hash from shape.
*/
int calculateHashLen() {
  int len = 0;
  for (int i = 0; shape[i] != '\0'; i++) {
    if (shape[i] == '0') {
      len++;
    }
  }
  return len;
}

/*******************************/
/// MAIN
/*******************************/

/*
  Program entry point.
*/
int main(void) {
  scanf ("%s", reference);
  scanf ("%s", shape);
  scanf ("%s", query);

  n = strlen(reference);
  m = strlen(query);
  shapeLen = strlen(shape);
  hashLen = calculateHashLen();
  k = shapeLen - hashLen;
  convertBasesValue();
  constructIndex();
  calculateBlocks();

  clock_t timeStart = clock();
  for (int i = 0; i + shapeLen <= m; i++) {
    findMathces(query, i, i + shapeLen);
  }
  clock_t timeEnd = clock();
  //printf ("%.3f\n", ((double)timeEnd - timeStart) / CLOCKS_PER_SEC);

  return 0;
}
