/**
  Checker for whether output from suffix array is subset from perfect hash.

  It has no purpose now becase suffix array also allows insertion+missmatch errors.
*/

#include <cstdio>
#include <map>
using namespace std;

map<int, int> M1;
map<int, int> M2;

/*
  Checks if first map is subset of second map.
*/
bool checkIfSubset(map<int, int> &_subset, map<int, int> _set) {
  for (map<int, int>::iterator it = _subset.begin(); it != _subset.end(); it++) {
    if (it->second > _set[it->first]) {
      printf ("key: %d, in subset freq: %d, in set freq: %d\n", it->first, it->second, _set[it->first]);
      return false;
    }
  }
  return true;
}

/*
  Reads all position from file f in given map M.
*/
void readPositions(FILE *f, int size, map<int, int> &M) {
  for (int i = 0; i < size; i++) {
    int a;
    fscanf(f, "%d", &a);
    M[a]++;
  }
}

/*
  Reads both files and calls methods for checking if f2 is subset of f1.
*/
bool readFile(FILE *f1, FILE *f2) {
  int size1, size2;
  while(fscanf(f1, "%d", &size1) == 1) {
    readPositions(f1, size1, M1);

    fscanf (f2, "%d", &size2);
    readPositions(f2, size2, M2);

    if (!checkIfSubset(M2, M1)) {
      return false;
    }
    M1.clear();
    M2.clear();
  }

  return true;
}

/*
  Program entry point. Receives two arguments from command line: output file from perfect hash and output file from
  suffix array, in this order.
*/
int main(int argc, char** argv) {
  /// input files should be in same folder
  FILE *f1 = fopen(argv[1], "r"); /// this should be ours output
  FILE *f2 = fopen(argv[2], "r"); /// this should be yours output

  if (readFile(f1, f2)) {
    printf ("CORRECT!\n");
  } else {
    printf ("%s is not subset of %s!\n", argv[2], argv[1]);
    return 1;
  }

  return 0;
}
